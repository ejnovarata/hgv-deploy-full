#!/bin/bash
export INSTALL_LOG=/tmp/install.log

source /root/ssinclude-1

# <UDF name="my_hostname" Label="Hostname" example="example.com OR localhost"/>
# <UDF name="wp_site_name" Label="Wordpress Site Name" default="My Site" example="My Site"/>
# <UDF name="wp_db" Label="Wordpress Database" default="wordpress" example="wordpress"/>
# <UDF name="wp_db_user" Label="Wordpress Database User" default="wpuser" example="wpuser"/>
# <UDF name="wp_db_password" Label="Wordpress Database Password" default="d4tab453p455w0rd" example="d4tab453p455w0rd"/>
# <UDF name="wp_admin_user" Label="Wordpress Admin User" default="admin" example="admin"/>
# <UDF name="wp_admin_user_password" Label="Wordpress Admin User" default="password" example="password"/>

if [ ! -d ~/config ]
then
cat << EOF > ~/config
#UDF Variables From First Installed
MY_HOSTNAME="${MY_HOSTNAME}"
WP_DB="${WP_DB}"
WP_DB_USER="${WP_DB_USER}"
WP_DB_PASSWORD="${WP_DB_PASSWORD}"
WP_ADMIN_USER="${WP_ADMIN_USER}"
WP_ADMIN_USER_PASSWORD="${WP_ADMIN_USER_PASSWORD}"
WP_SITE_NAME="${WP_SITE_NAME}"

EOF
fi

LOG=~/install.log

function log(){
 echo "[`date +%H:%M:%S`]$1" >> ${LOG}
}

if [ -d ~/config ]
then
  source ~/config
fi



##
# Logging
##
function init_log(){
  if [ -d ${INSTALL_LOG} ]
  then
    rm ${INSTALL_LOG}
  fi
  exec > ${INSTALL_LOG} 2>&1
}

##Init logging
init_log



function log()
{
  echo -e "[`date +%H:%M:%S`] (`basename $0`:${FUNCNAME[1]}): $@"
}

##
# is package installed
# $1 = package name
##
function is_installed(){
  dpkg -s $1
  local __installed_status=$?
  if [ $__installed_status = 0 ]
  then
    return 0;
  else
    return 1;
  fi
}

##
# install package
# $@ packages
##
function apt_install(){
  for pkg in "$@"
  do
    if ! is_installed $pkg
    then
      apt-get install -y $pkg
    fi
  done
}

##
# uninstall package
# $@ packages
##
function apt_remove(){
  for pkg in "$@"
  do
    if is_installed $pkg
    then
      apt-get remove -y $pkg
    fi
  done
  apt-get autoremove
}

function apt_update(){
  apt-get update -y
}

##
# add repository
##
function apt_add_repo(){
  add-apt-repository -y $1
  apt_update
}
##
# remove repository
##
function apt_remove_repo(){
  apt-add-repository --remove -y $1
  apt_update
}


apt_install aptitude
apt_install software-properties-common

apt_add_repo ppa:ansible/ansible

postfix_install_loopback_only

apt_install git
apt_install ansible

if [ ! -d ~/hgv-deploy-full ]
then
  echo "Running git"
  git clone https://github.com/zach-adams/hgv-deploy-full.git ~/hgv-deploy-full
  echo "git done!"
fi



cat << EOF > ~/hgv-deploy-full/hosts
  [all]
  ${MY_HOSTNAME}
EOF

log "Copy host..."

cp ~/hgv-deploy-full/host_vars/yourhostname.com ~/hgv-deploy-full/host_vars/${MY_HOSTNAME}

log "Copy host Done!"

cat << EOF > ~/hgv-deploy-full/host_vars/${MY_HOSTNAME}
---

  admin_email: ${ADMIN_EMAIL}
  hostname: ${HOSTNAME}
  domain: ${DOMAIN}
  db: ${WP_DB}
  db_user: ${WP_DB_USER}
  db_user_password: ${WP_DB_PASSWORD}
  wp_admin: ${WP_ADMIN_USER}
  wp_admin_password: ${WP_ADMIN_USER_PASSWORD}
  wp_site_name: ${WP_SITE_NAME}

EOF

ansible-playbook -i ~/hgv-deploy-full/hosts ~/hgv-deploy-full/playbook.yml -c local

log "Restarting Services..."

service nginx restart
service varnish restart

log "Done!"

# rm -r ~/hgv-deploy-full
