#!/bin/bash
###
## MySQL backup script
## Eric Johansson <ejohansson@novarata.com
###
BACKUP_FOLDER=/root/database_backups
DB_USER={{ db_user }}
DB_PASSWORD={{ db_user_password }}
DB_SCHEMA={{ db }}
BACKUP_NAME=${DB_SCHEMA}_backup_$(date +"%Y-%m-%d_%H:%M:%S")

## create backup folder
mkdir -p ${BACKUP_FOLDER}/${DB_SCHEMA}/older

## Move backups to old folder and create a new backup
mv ${BACKUP_FOLDER}/${DB_SCHEMA}/*.sql.gz ${BACKUP_FOLDER}/${DB_SCHEMA}/older/.
mysqldump -u ${DB_USER} -p${DB_PASSWORD} ${DB_SCHEMA} | gzip > ${BACKUP_FOLDER}/${DB_SCHEMA}/${BACKUP_NAME}.sql.gz

## remove backups older than 7 days
find ${BACKUP_FOLDER}/${DB_SCHEMA}/older/* -mtime +7 -exec rm {} \;
