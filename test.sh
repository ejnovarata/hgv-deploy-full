#!/bin/bash

echo "

 -------------------------  -----------------
|   _    _                ||  __      __     |
|  | |  | |               ||  \ \    / /     |
|  | |__| |  __           ||   \ \  / /      |
|  |  __  |/ _  \         ||    \ \/ /       |
|  | |  | | (_| |         ||     \  /        |
|  |_|  |_|\__, |         ||      \/         |
|           __/ |___  __  ||        ___ ____ |
|          |___/( _ )/  \ ||       |_  )__ / |
|               / _ \ () |||        / / |_ \ |
|               \___/\__/ ||       /___|___/ |
 -------------------------  -----------------

"

set -e
LSB=`lsb_release -r | awk {'print $2'}`

ansible_version=`dpkg -s ansible 2>&1 | grep Version | cut -f2 -d' '`
echo
echo "Ansible installed ($ansible_version)"

ANS_BIN=`which ansible-playbook`

if [[ -z $ANS_BIN ]]
    then
    echo "Whoops, can't find Ansible anywhere. Aborting run."
    echo
    exit
fi

# More continuous scroll of the ansible standard output buffer
export PYTHONUNBUFFERED=1
export ANSIBLE_FORCE_COLOR=true

. config.androidatl

echo "${SHOSTNAME}" >> hosts

cat << EOF > host_vars/${SHOSTNAME}
---

  admin_email: ${ADMIN_EMAIL}
  hostname: ${SHOSTNAME}
  domain: ${DOMAIN}
  db: ${WP_DB}
  db_user: ${WP_DB_USER}
  db_user_password: ${WP_DB_PASSWORD}
  wp_admin: ${WP_ADMIN_USER}
  wp_admin_password: ${WP_ADMIN_USER_PASSWORD}
  wp_site_name: ${WP_SITE_NAME}

EOF


echo
echo "Checking Playbook."
echo

$ANS_BIN --syntax-check --list-tasks -i hosts playbook.yml -c local

echo
echo "Done!"
echo
